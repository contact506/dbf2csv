package com.ciriscr.dbf2csv

import scala.collection.mutable.Map

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 23/07/13
 * Time: 11:13 AM
 */

object Main {

  val params = Map.empty[Parametros.Value, Any]

  def main(a: Array[String]) {
    val it = a.toIterator
    while (it.hasNext){
      val arg = it.next()
      arg match {
        case "-d" => params += (Parametros.delimitador -> it.next().charAt(0))
        case "-o" => params += (Parametros.salida -> it.next())
        case "-h" => params += (Parametros.header -> true)
        case e @ _ => params += (Parametros.entrada -> e)
      }
    }

    new DBFReader(params.get(Parametros.entrada).map(_.toString).getOrElse(""),
                  params.get(Parametros.salida).map(_.toString).getOrElse(""),
                  params.get(Parametros.header).map(_.toString.toBoolean).getOrElse(false),
                  params.get(Parametros.delimitador).map(_.toString.charAt(0)).getOrElse(';')).read()
  }

}

object Parametros extends Enumeration {
  val entrada = Value
  val delimitador = Value
  val salida = Value
  val header = Value
}
