package com.ciriscr.dbf2csv

import scala.collection.mutable.Map
import org.joda.time.{LocalDate, LocalTime, DateTime}

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 23/07/13
 * Time: 11:17 AM
 */

trait Writter {
  val filePath: String

  protected val log = new Logger(filePath)
}

class CSVWritter(val filePath:String, encabezado: Seq[Encabezado], val incluirEncabezado: Boolean)
                (splitter: Char = ',', stringDelimiter: Char = '"', dateFormat: String = "YYYY-MM-dd", hourFormat: String = "hh:mm:ss") extends Writter {

  private val linea: Map[String, Any] = Map.empty[String, Any]
  if (incluirEncabezado) escribir(encabezado.map(_.nombre).toList:_*)
  llenarMapa

  def cerrar() {log.cerrar}
  private def escribirL(linea: String) {log.escribir(linea)}

  private def escribir(campos: Any*){
    escribirL(campos.map(_ match{
      case d:LocalDate => s"$stringDelimiter${d.toString(dateFormat)}$stringDelimiter"
      case t:LocalTime => s"$stringDelimiter${t.toString(hourFormat)}$stringDelimiter"
      case d:DateTime => s"$stringDelimiter${d.toString(s"$dateFormat $hourFormat")}$stringDelimiter"
      case s:String => s"$stringDelimiter$s$stringDelimiter"
      case c:Char => s"$stringDelimiter$c$stringDelimiter"
      case f @ _ => f.toString
    }).mkString(splitter.toString()))
  }

  private def escribir() {
    escribir(encabezado.map(e => linea.get(e.nombre).get).toList:_*)
  }

  def addCampo(campo: String, valor: Any) {
    linea.get(campo) match {
      case Some(x) => linea.update(campo, valor)
      case _ => linea += campo -> valor
    }
  }

  def addCampo(campo: (String, Any)*) { campo.foreach(addCampo) }
  def addCampo(campo: (String, Any)) { addCampo(campo._1, campo._2) }

  private def llenarMapa() {
    linea.clear()
    encabezado.map(e => e.nombre -> e.valorDefault).foreach(linea += _)
  }

  def nuevaLinea() {
    escribir
    llenarMapa
  }

}

class Encabezado(val nombre: String, val valorDefault: Any)
