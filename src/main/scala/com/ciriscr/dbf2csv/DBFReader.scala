package com.ciriscr.dbf2csv

import java.io.FileInputStream
import scala.annotation.tailrec
import com.linuxense.javadbf.DBFField

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 23/07/13
 * Time: 11:29 AM
 */

class DBFReader(archivoEntrada: String, archivoSalida: String, withHeader: Boolean, splitter: Char) {
  import com.linuxense.javadbf.{DBFReader => DbfReader}

  val reader = new DbfReader(new FileInputStream(archivoEntrada))
  lazy val numberOfFields = reader.getFieldCount()
  val fields = (0 until reader.getFieldCount).map(i => reader.getField(i))
  lazy val encabezado = fields.map(f => new Encabezado(f.getName, ""))
  val writter = new CSVWritter(archivoSalida, encabezado, withHeader)(splitter)

  def read() {
    @tailrec
    def repite(row: Array[AnyRef]) {
      if (row != null) {
        val campos = row.toSeq.zip(fields).withFilter(_._1 != null).map(mappear)
        writter.addCampo(campos:_*)
        writter.nuevaLinea()
        repite(reader.nextRecord())
      }
    }
    def mappear(f: (AnyRef, DBFField)) = f._2.getDataType match {
      case 'C' => f._2.getName -> f._1.toString.trim
      case 'N' | 'F' => f._2.getName -> f._1.formatted("%1$.0f").trim
      case 'L' => f._2.getName -> f._1.toString.toBoolean
      case 'D' => f._2.getName -> f._1.toString.trim
    }

    repite(reader.nextRecord())
  }
}
